[SENSOR]

	boolean online;
	float current_degrees;

	int object_indice;
	Depth_Model depth_model;

	// HARDWARE

	void init();
	void shutdown();
	void tilt(float degrees);
	void startDepthStream();
		// Devolver mapa de profundidade do objeto cujo indice � indicado
		// Motion tracking do objeto?
	void stopDepthStream();
	
	// GETS
	// SETS
	// METHODS

	void addMapToDepthModel (Depth_Map a);
	3D_Cloud convertDepthToCloud ();
	3D_Model convertCloudToModel (3D_Cloud a);
		// Gameobject?
		// Criacao de uma mesh tendo em conta processo de triangulacao

[DEPTH_MAP]
[DEPTH_MODEL]
[3D_CLOUD]
[3D_MODEL]