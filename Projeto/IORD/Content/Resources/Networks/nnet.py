import os
import glob
import json
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from keras import backend as K
from keras.utils import np_utils
from keras.optimizers import SGD
from keras.models import Sequential, load_model, model_from_json, model_from_yaml
from keras.layers import Dense, Activation, Dropout, Flatten, Conv3D, MaxPooling3D
from sklearn.preprocessing import LabelEncoder

seed = 9
np.random.seed(seed)

'''
Class that gives the user capability to create a Multi-Layer Perceptron
'''
class MLP:
	'''
	Creates a new model of a Neural network using the layers with the number
		of nodes provided in the array
	Receives an array with the number of nodes and activation function by layer
	Returns the created model
	'''
	@staticmethod
	def create_model(input_dim, act, inits, nodes, regression = False):
		model = Sequential()
		for i in range(len(nodes)) :
			if i == 0:
				model.add(Dense(nodes[i], input_dim = input_dim, kernel_initializer = inits[i]))
				try:
					if isinstance(act[i], str):
						model.add(Activation(act[i]))
					else:
						model.add(act[i])
				except:
					pass
			else:
				if isinstance(nodes[i], str):
					if nodes[i] == 'dropout':
						model.add(Dropout(act[i]))
				else:
					model.add(Dense(nodes[i], kernel_initializer = inits[i]))
					try:
						if isinstance(act[i], str):
							model.add(Activation(act[i]))
						else:
							model.add(act[i])
					except:
						pass
		return model

'''
Class that gives the user capability to create a Convolutional Neural Network
'''
class InvalidDimNumber(Exception):
	def __init__(self, value):
		self.value = value
	def __str__(self):
		return repr(self.value)

class DNN:
	'''
	........
	'''
	def __init__(self, nb_channels, img_dim_nr, img_dim):
		# Input Image Dimensions (1,2 or 3 dimensions)
		self.nb_channels = nb_channels
		self.img_dim = [0,0,0]
		for i in range(img_dim_nr):
			self.img_dim[i] = img_dim[i]
		if K.image_dim_ordering() == 'th':
			if img_dim_nr == 1:
				img_dim_nr = (self.nb_channels, img_dim[0])
			elif img_dim_nr == 2:
				self.input_shape = (self.nb_channels, img_dim[0], img_dim[1])
			elif img_dim_nr == 3:
				self.input_shape = (self.nb_channels, img_dim[0], img_dim[1], img_dim[2])
			else:
				raise InvalidDimNumber(img_dim_nr + ' is an invalid number of dimensions. Use 1,2 or 3 dimensions.\n')
		else:
			if img_dim_nr == 1:
				self.input_shape = (img_dim[0], self.nb_channels)
			elif img_dim_nr == 2:
				self.input_shape = (img_dim[0], img_dim[1], self.nb_channels)
			elif img_dim_nr == 3:
				self.input_shape = (img_dim[0], img_dim[1], img_dim[2], self.nb_channels)
			else:
				raise InvalidDimNumber(img_dim_nr + ' is an invalid number of dimensions. Use 1,2 or 3 dimensions.\n')

	'''
	........
	'''
	def create_model(self, layers, params, acts):
		model = Sequential()
		for i in range(len(layers)):
			if i == 0:
				if layers[i] == 'conv':
					model.add(Conv3D(params[i][0], (params[i][1],params[i][2],params[i][3]),
							 input_shape = self.input_shape))
				elif layers[i] == 'maxpool':
					model.add(MaxPooling3D(pool_size = params[i][0],
								   input_shape = self.input_shape))
				elif layers[i] == 'dense':
					model.add(Dense(params[i][0], kernel_initializer = params[i][1],
							  input_shape = self.input_shape))
				elif layers[i] == 'flatten':
					model.add(Flatten(input_shape = self.input_shape))
				elif layers[i] == 'dropout':
					model.add(Dropout(params[i][0], input_shape = self.input_shape))
			else:
				if layers[i] == 'conv':
					model.add(Conv3D(params[i][0], (params[i][1],params[i][2],params[i][3])))
				elif layers[i] == 'maxpool':
					model.add(MaxPooling3D(pool_size = params[i][0]))
				elif layers[i] == 'dense':
					model.add(Dense(params[i][0], kernel_initializer = params[i][1]))
				elif layers[i] == 'flatten':
					model.add(Flatten())
				elif layers[i] == 'dropout':
					model.add(Dropout(params[i][0]))
			if (acts[i] != ''):
				if isinstance(acts[i], str):
					model.add(Activation(acts[i]))
				else:
					model.add(acts[i])
		return model

'''
Class with generic operations in all types of Neural Networks
'''
class Generic:
	'''
	Compiles a model using loss calculation, optimizer and metrics to calculate
	Receives the loss calculation function, the optimizer to use and the metrics to calculate
	Returns the model compiled
	'''
	@staticmethod
	def compile_model(model, loss = 'categorical_crossentropy',
					  optimizer = 'adam', metrics = ['accuracy']):
		model.compile(loss = loss, optimizer = optimizer, metrics = metrics)
		return model

	'''
	Trains a model
	Receives the number of epochs to train, the batch_size and the arrays of parameters and labels
	Returns the history of training (loss by epoch of training)
	'''
	@staticmethod
	def fit_model(model, X_train, Y_train, epochs = 50,
				  batch_size = 5, validation_data = None, validation_split = None, verbose = 1):
		if validation_data != None:
			history = model.fit(X_train, Y_train, epochs = epochs,
								batch_size = batch_size, validation_data = validation_data,
								verbose = verbose)
		elif validation_split != None:
			history = model.fit(X_train, Y_train, epochs = epochs,
								batch_size = batch_size, validation_split = validation_split,
								verbose = verbose)
		return history

	'''
	...........
	'''
	@staticmethod
	def predict(model, to_predict, mode = 'probs'):
		if mode == 'probs':
			probs = model.predict_proba(to_predict, verbose=0)
			return str(probs)
		elif mode == 'classes':
			classes = model.predict_classes(to_predict, verbose=0)
			return str(classes)

	'''
	Predict values using the model and prints it on screen
	'''
	@staticmethod
	def model_print_predictions(model, X_test, Y_test, length, mean = None, sd = None):
		pred = model.predict(X_test)
		diff = []
		racio = []
		for i in range(length):
			if mean is not None:
				if sd is not None:
					# Y = med + (Z*sd)
					Y_test[i] = mean + (Y_test[i] * sd)
					pred[i] = mean + (pred[i] * sd)
			racio.append( (Y_test[i] / pred[i]) - 1)
			diff.append( abs(Y_test[i] - pred[i]))
			print('Valor: %f ---> Previsao: %f Diff: %f Racio: %f' % (Y_test[i], pred[i], diff[i], racio[i]))

	'''
	Prints a graph showing the evolution of the accuracy of the network
	'''
	@staticmethod
	def print_history_accuracy(history):
		plt.plot(history.history['acc'])
		plt.plot(history.history['val_acc'])
		plt.title('model accuracy')
		plt.ylabel('accuracy')
		plt.xlabel('epoch')
		plt.legend(['train', 'test'], loc = 'upper left')
		plt.show()

	'''
	Prints a graph showing the evolution of the loss
	'''
	@staticmethod
	def print_history_loss(history):
		plt.plot(history.history['loss'])
		plt.plot(history.history['val_loss'])
		plt.title('model loss')
		plt.ylabel('loss')
		plt.xlabel('epoch')
		plt.legend(['train', 'test'], loc = 'upper left')
		plt.show()

	'''
	Prints a model into a Graphviz model
	Revceives the model to print and the file where it is going to print
	NOTE: Needs to have 'dot' installed
	'''
	@staticmethod
	def print_model(model, fich):
		from keras.utils import plot_model
		plot_model(model, to_file = fich, show_shapes = True, show_layer_names = True)

	'''
	Returns the metrics defined in the model's compilation
	'''
	@staticmethod
	def model_evaluate(model, X_test, Y_test):
		scores = model.evaluate(X_test, Y_test)
		return (model.metrics_names, scores)

	'''
	Saves a model into JSON format
	'''
	@staticmethod
	def save_model_json(model, fich):
		model_json = model.to_json()
		with open(fich, "w") as json_file:
			json_file.write(model_json)

	'''
	Saves a model into YAML format
	'''
	@staticmethod
	def save_model_yaml(model, fich):
		model_yaml = model.to_yaml()
		with open(fich, "w") as yaml_file:
			yaml_file.write(model_yaml)

	'''
	Saves the weights of the network in HDF5 format
	'''
	@staticmethod
	def save_weights_hdf5(model, fich):
		model.save_weights(fich)

	'''
	Reads a model from JSON format
	'''
	@staticmethod
	def load_model_json(fich):
		json_file = open(fich, 'r')
		loaded_model_json = json_file.read()
		json_file.close()
		loaded_model = model_from_json(loaded_model_json)
		return loaded_model

	'''
	Reads a model from YAML format
	'''
	@staticmethod
	def load_model_yaml(fich):
		yaml_file = open(fich, 'r')
		loaded_model_yaml = yaml_file.read()
		yaml_file.close()
		return model_from_yaml(loaded_model_yaml)

	'''
	Reads the weights of the network from HDF5 format
	'''
	@staticmethod
	def load_weights_hdf5(model, fich):
		model.load_weights(fich)

'''
Class with utilities to treat datasets
'''
class Datasets:
	'''
	..........
	'''
	@staticmethod
	def exists(filename):
		return os.path.exists(filename)

	'''
	Read a CSV dataset file
	Receives the name of the file and the column index where is the label to classify
	Return two arrays, the parameters to feed the network and the labels array
	'''
	@staticmethod
	def read_csv_dataset(filename, cols):
		data = pd.read_csv(filename, header = 0, names = cols)
		df = pd.DataFrame(data)
		return df

	'''
	Generates train and test partitions from a dataset
	Receives the dataset
	Returns the data partitions
	'''
	@staticmethod
	def generate_partitions(dataset, train_split = 0.67, categorical = False):
		att = len(dataset.columns)
		# split into train and test data frames
		aux = dataset.as_matrix()
		aux = np.array(aux)
		split_point = int(round(train_split * aux.shape[0]))
		train = aux[0 : split_point, : ]
		# np.random.shuffle(train)
		test = aux[split_point : len(aux), : ]
		# np.random.shuffle(test)
		# get labels
		X_train = train[:, 0:train.shape[1] - 1]
		Y_train = train[:, -1]
		X_test = test[:, 0:test.shape[1] - 1]
		Y_test = test[:, -1]
		X_train = X_train.astype('float32')
		X_test = X_test.astype('float32')
		# reshape frames
		# X_train = np.reshape(X_train, (X_train.shape[0], X_train.shape[1]))
		# X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1]))
		if categorical == True:
			# convert integers to dummy variables (i.e. one hot encoded)
			Y_train = np_utils.to_categorical(Y_train)
			Y_test = np_utils.to_categorical(Y_test)
		return (X_train, Y_train), (X_test, Y_test)

	'''
	...........
	'''
	@staticmethod
	def read_iord_model_single(filename, img_rows, img_cols, img_depth):
		# Prepare data to predict
		X_test = []
		with open(filename) as f:
			file = f.readlines()
			json_file = '\n'.join(file)
			content = json.loads(json_file)
			occupancy = content['model']['values']
			form = []
			for value in occupancy:
				form.append(int(value))
			final_model = [ [ [ 0 for i in range(img_depth) ]
								  for j in range(img_cols) ]
								  for k in range(img_rows) ]
			# Format as 3D Matrix
			a = 0
			for i in range(img_rows):
				for j in range(img_cols):
					for k in range(img_depth):
						final_model[i][j][k] = form[a]
						a = a + 1
			X_test.append(final_model)
		# Transform to Numpy Array
		X_test = np.array(X_test)
		# Resize to fit Network Input Shapes
		if K.image_dim_ordering() == 'th':
			X_test = X_test.reshape(X_test.shape[0], 1, img_rows, img_cols, img_depth)
		else:
			X_test = X_test.reshape(X_test.shape[0], img_rows, img_cols, img_depth, 1)
		return X_test

	'''
	.............
	'''
	@staticmethod
	def read_iord_train_sets_single(path, img_rows, img_cols, img_depth):
		# Prepare data to train
		i, nb_classes = 0, 0
		X_train, Y_train = [], []
		X_test, Y_test = [], []
		# Read files
		path = path + "*.iordts"
		nb_classes = -1
		for filename in glob.glob(path):
			with open(filename) as f:
				file = f.readlines()
				json_file = '\n'.join(file)
				content = json.loads(json_file)
				count = content['model']['count']
				for i in range(count):
					occupancy = content['model']['set'][str(i)]
					form = []
					for value in occupancy:
						form.append(int(value))
					final_model = [ [ [ 0 for i in range(img_depth) ]
										  for j in range(img_cols) ]
										  for k in range(img_rows) ]
					# Format as 3D Matrix
					a = 0
					for b in range(img_rows):
						for c in range(img_cols):
							for d in range(img_depth):
								final_model[b][c][d] = form[a]
								a = a + 1
					if i < 0.66 * count:
						X_train.append(final_model)
						Y_train.append(content['model']['label'])
					else:
						X_test.append(final_model)
						Y_test.append(content['model']['label'])
					if content['model']['label'] > nb_classes:
						nb_classes = content['model']['label']
		nb_classes+=1
		# Transform to Numpy Array
		X_train = np.array(X_train)
		Y_train = np.array(Y_train)
		X_test = np.array(X_test)
		Y_test = np.array(Y_test)
		# Resize to fit Network Input Shapes
		if K.image_dim_ordering() == 'th':
			X_train = X_train.reshape(X_train.shape[0], 1, img_rows, img_cols, img_depth)
			X_test = X_test.reshape(X_test.shape[0], 1, img_rows, img_cols, img_depth)
		else:
			X_train = X_train.reshape(X_train.shape[0], img_rows, img_cols, img_depth, 1)
			X_test = X_test.reshape(X_test.shape[0], img_rows, img_cols, img_depth, 1)
		Y_train = np_utils.to_categorical(Y_train, nb_classes)
		Y_test = np_utils.to_categorical(Y_test, nb_classes)
		return (X_train, Y_train), (X_test, Y_test), nb_classes

	'''
	...........
	'''
	@staticmethod
	def read_iord_model_normals(filename, img_rows, img_cols, img_depth):
		# Prepare data to predict
		X_test = []
		with open(filename) as f:
			file = f.readlines()
		json_file = '\n'.join(file)
		content = json.loads(json_file)
		occupancy_x = content['model']['x']
		occupancy_y = content['model']['y']
		occupancy_z = content['model']['z']
		form_x = []
		form_y = []
		form_z = []
		for value in occupancy_x:
			form_x.append(int(value))
		for value in occupancy_y:
			form_y.append(int(value))
		for value in occupancy_z:
			form_z.append(int(value))
		final_model = [ [ [ [ 0 for i in range(3) ]
								for j in range(img_depth) ]
								for k in range(img_cols) ]
								for l in range(img_rows) ]
		# Format as 3D Matrix
		a = 0
		for b in range(img_rows):
			for c in range(img_cols):
				for d in range(img_depth):
					final_model[b][c][d][0] = form_x[a]
					final_model[b][c][d][1] = form_y[a]
					final_model[b][c][d][2] = form_z[a]
					a = a + 1
		X_test.append(final_model)
		# Transform to Numpy Array
		X_test = np.array(X_test)
		# Resize to fit Network Input Shapes
		if K.image_dim_ordering() == 'th':
			X_test = X_test.reshape(X_test.shape[0], 3, img_rows, img_cols, img_depth)
		else:
			X_test = X_test.reshape(X_test.shape[0], img_rows, img_cols, img_depth, 3)
		return X_test

	'''
	.............
	'''
	@staticmethod
	def read_iord_train_sets_normals(path, img_rows, img_cols, img_depth):
		# Prepare data to train
		i, nb_classes = 0, 0
		X_train, Y_train = [], []
		X_test, Y_test = [], []
		# Read files
		path = path + "*.iordts"
		for filename in glob.glob(path):
			with open(filename) as f:
				file = f.readlines()
			json_file = '\n'.join(file)
			content = json.loads(json_file)
			count = content['model']['count']
			for i in range(count):
				occupancy_x = content['model']['set'][str(i)]['x']
				occupancy_y = content['model']['set'][str(i)]['y']
				occupancy_z = content['model']['set'][str(i)]['z']
				form_x = []
				form_y = []
				form_z = []
				for value in occupancy_x:
					form_x.append(int(value))
				for value in occupancy_y:
					form_y.append(int(value))
				for value in occupancy_z:
					form_z.append(int(value))
				final_model = [ [ [ [ 0 for i in range(3) ]
									    for j in range(img_depth) ]
									    for k in range(img_cols) ]
										for l in range(img_rows) ]
				# Format as 3D Matrix
				a = 0
				for b in range(img_rows):
					for c in range(img_cols):
						for d in range(img_depth):
							final_model[b][c][d][0] = form_x[a]
							final_model[b][c][d][1] = form_y[a]
							final_model[b][c][d][2] = form_z[a]
							a = a + 1
				if i < 0.66 * count:
					X_train.append(final_model)
					Y_train.append(content['model']['label'])
				else:
					X_test.append(final_model)
					Y_test.append(content['model']['label'])
				if content['model']['label'] > nb_classes:
					nb_classes = content['model']['label']
		nb_classes+=1
		# Transform to Numpy Array
		X_train = np.array(X_train)
		Y_train = np.array(Y_train)
		X_test = np.array(X_test)
		Y_test = np.array(Y_test)
		# Resize to fit Network Input Shapes
		if K.image_dim_ordering() == 'th':
			X_train = X_train.reshape(X_train.shape[0], 3, img_rows, img_cols, img_depth)
			X_test = X_test.reshape(X_test.shape[0], 3, img_rows, img_cols, img_depth)
		else:
			X_train = X_train.reshape(X_train.shape[0], img_rows, img_cols, img_depth, 3)
			X_test = X_test.reshape(X_test.shape[0], img_rows, img_cols, img_depth, 3)
		Y_train = np_utils.to_categorical(Y_train, nb_classes)
		Y_test = np_utils.to_categorical(Y_test, nb_classes)
		return (X_train, Y_train), (X_test, Y_test), nb_classes
