import sys
import os
path = sys.argv[1]
os.chdir(path)
import math
sys.path.insert(0, '../../../')
sys.stderr = open('null', 'w')
from nnet import Datasets, Generic, DNN
import numpy as np

# environment settings
os.environ['TF_CPP_MIN_LOG_LEVEL']='3'
np.set_printoptions(threshold = np.inf)

# fix random seed for reproducibility
seed = 1
net_filename = '../Weights/LE_NET_Density.iordcore'
weights_filename = '../Weights/weights_LE_NET_Density.iordcore'

if Datasets.exists(net_filename):
	# load network model
	model = Generic.load_model_json(net_filename)
	Generic.compile_model(model, loss = 'categorical_crossentropy', optimizer = 'adam')
	Generic.load_weights_hdf5(model, weights_filename)

	# load test model
	filename = '../Models/Test/' + sys.argv[2]
	test_model = Datasets.read_iord_model_single(filename, 25, 25, 25)

	# predict probabilities
	print(model.predict_proba(test_model, verbose = 0))
else:
	print(-1)
