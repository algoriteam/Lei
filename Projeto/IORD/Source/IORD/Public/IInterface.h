// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <string>
#include <array>

#include <iostream>
#include <sstream>
#include <fstream>

#include <cstdio>

#include <filesystem>

#include "GameFramework/Actor.h"
#include "IInterface.generated.h"

UCLASS()
class IORD_API AIInterface : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AIInterface();

	UFUNCTION(BlueprintCallable)
	TArray<FString> listCloudModels();
	UFUNCTION(BlueprintCallable)
	TArray<FString> listTestModels(int type);
	UFUNCTION(BlueprintCallable)
	TArray<FString> listTrainSets(int type, int mode);
	UFUNCTION(BlueprintCallable)
	TArray<FString> listTrainDescriptions(int type, int mode);

	UFUNCTION(BlueprintCallable)
	void deleteSelectedModel(int type, FString filename);
	UFUNCTION(BlueprintCallable)
	void deleteSelectedSet(int type, int mode, FString filename);
	UFUNCTION(BlueprintCallable)
	void deleteSelectedCloudPoint(FString filename);

	UFUNCTION(BlueprintCallable)
	int train(int type, int mode);
	UFUNCTION(BlueprintCallable)
    TMap<FString, float> test(int type, FString model);

	UFUNCTION(BlueprintCallable)
	void drawCoordinates();


private:
	void deleteFile(FString path);
	TArray<FString> listFiles(FString path);

	int readTrainMode(int type);
	void writeTrainMode(int type, int mode);

	std::string exec(const char* cmd);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
