// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "NuiApi.h"
#include "NuiImageCamera.h"
#include "NuiSensor.h"

#include "GameFramework/Actor.h"
#include "Sensor.generated.h"

UCLASS()
class IORD_API ASensor : public AActor
{
	GENERATED_BODY()

	int width;
	int height;

	HANDLE depthStream;            // The identifier of the Kinect's DEPTH Camera
	INuiSensor* sensor;            // The kinect sensor
	bool active;

	int mode;
	int state;

	// Points
	FColor backColor;
	TArray<FVector> back;

	FColor objectColor;
	TArray<TArray<FVector>> object;

	// Tolerāncia de distāncia entre pontos
	float tolerance;
	
public:	
	// Sets default values for this actor's properties
	ASensor();

	UFUNCTION(BlueprintCallable)
	int countKinect();

	// CONFIG
	UFUNCTION(BlueprintCallable)
	void clear();
	UFUNCTION(BlueprintCallable)
	void initKinect(FColor backgroundColor, FColor objectColor, bool nearMode, float tolerance);

	// DRAW
	UFUNCTION(BlueprintCallable)
	void drawBackground();
	UFUNCTION(BlueprintCallable)
	void drawObject();

	// PHOTOS
	UFUNCTION(BlueprintCallable)
	void getBackground();
	UFUNCTION(BlueprintCallable)
	void takePhoto(bool rotate, float angle);

	// SAVE
	UFUNCTION(BlueprintCallable)
	void saveModel(FString filename, FString description);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	void drawList(TArray<FVector> a);

	TArray<USHORT> getDepth();
	TArray<FVector> getPoints();

	TArray<FVector> center(TArray<FVector> points);
	TArray<FVector> rotate(TArray<FVector> points, float angle);
	TArray<FVector> translate(TArray<FVector> points, float x, float y, float z);

	FVector centroid(TArray<FVector> a);

};
