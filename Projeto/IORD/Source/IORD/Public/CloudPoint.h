// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

/**
 * 
 */
class IORD_API CloudPoint
{
	// List of points
	TArray<FVector> cloud;

public:
	CloudPoint();
	~CloudPoint();

	TArray<FVector> getCloud();

	void addPoint(FVector p);
	void addPoints(TArray<FVector> p);
	void addPoints(CloudPoint c);

	void removePoint(FVector p);
	void removePoints(CloudPoint c);

	int numPoints();

	CloudPoint applyNoise(float level);

	FVector getCentroid();
	float getMaximum();

	void clear();
};
