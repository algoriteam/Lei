// Fill out your copyright notice in the Description page of Project Settings.

#include "IORD.h"
#include "IPlane.h"

// ----- CONSTRUCTORS ----- //
IPlane::IPlane()
{
	// Nothing to do
}

IPlane::IPlane(FVector p, FVector c, FVector n) {
	this->position = p;
	this->centroid = c;
	this->normal = n;
}

IPlane::~IPlane()
{
	// Nothing to do
}

// ----- GETS ----- //
FVector IPlane::getPosition() {
	return this->position;
}

FVector IPlane::getCentroid() {
	return this->centroid;
}

FVector IPlane::IPlane::getNormal() {
	return this->normal;
}
