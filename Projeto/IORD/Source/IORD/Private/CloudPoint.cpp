// Fill out your copyright notice in the Description page of Project Settings.

#include "IORD.h"
#include "CloudPoint.h"

#include <ctime>
#include <cstdlib>

// ----- CONSTRUCTORS ----- //

CloudPoint::CloudPoint()
{
	// Nothing to do here
}

CloudPoint::~CloudPoint()
{
	// Clear memory
	this->clear();
}

// ----- METHODS ----- //

TArray<FVector> CloudPoint::getCloud()
{
	return this->cloud;
}

void CloudPoint::addPoint(FVector p)
{
	this->cloud.AddUnique(p);
}

void CloudPoint::addPoints(TArray<FVector> p)
{
	for (int i = 0; i < p.Num(); i++) {
		this->addPoint(p[i]);
	}
}

void CloudPoint::addPoints(CloudPoint c)
{
	TArray<FVector> aux = c.getCloud();
	for (int i = 0; i < aux.Num(); i++) {
		this->addPoint(aux[i]);
	}
}

void CloudPoint::removePoint(FVector p)
{
	this->cloud.Remove(p);
}

void CloudPoint::removePoints(CloudPoint c)
{
	TArray<FVector> aux = c.getCloud();
	for (int i = 0; i < aux.Num(); i++) {
		this->removePoint(aux[i]);
	}
}

int CloudPoint::numPoints() {
	return this->cloud.Num();
}

void CloudPoint::clear()
{
	this->cloud.Empty();
}

CloudPoint CloudPoint::applyNoise(float level) {
	srand(time(NULL));

	CloudPoint res = CloudPoint();
	float x, y, z;

	float max = level;
	float min = -level;
	float range = max - min;

	for (FVector p : this->cloud) {
		//This will generate a number from some arbitrary LOW to some arbitrary HIGH
		x = range * ((((float)rand()) / (float)RAND_MAX)) + min;
		y = range * ((((float)rand()) / (float)RAND_MAX)) + min;
		z = range * ((((float)rand()) / (float)RAND_MAX)) + min;

		res.addPoint(FVector(p.X + x, p.Y + y, p.Z + z));
	}

	return res;
}

FVector CloudPoint::getCentroid() {
	FVector sum = FVector(0.0f);
	FVector center;

	// Calculate points sum
	for (FVector v : this->cloud) {
		sum += v;
	}

	// Calculate points centroid
	center = sum * (1.0f / numPoints());

	return center;
}

float CloudPoint::getMaximum() {
	float max = 0.0f;
	float temp_max, abs_x, abs_y, abs_z;

	// Calculate points sum
	for (FVector v : this->cloud) {
		abs_x = FMath::Abs(v.X);
		abs_y = FMath::Abs(v.Y);
		abs_z = FMath::Abs(v.Z);

		temp_max = FMath::Max3(abs_x, abs_y, abs_z);
		
		if (temp_max > max)
			max = temp_max;
	}

	return max;
}
