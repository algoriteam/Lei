// Fill out your copyright notice in the Description page of Project Settings.

#include "IORD.h"
#include "IInterface.h"

// Representation modes
#define OCCUPANCY 0
#define DENSITY 1
#define NORMALS 2

// Model dataset
#define SIMPLE 0
#define ADVANCED 1
#define CUSTOM 2

// Sets default values
AIInterface::AIInterface()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AIInterface::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AIInterface::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// ---------------- //
// ----- LIST ----- //
// ---------------- //

// List all cloudpoint models
TArray<FString> AIInterface::listCloudModels() {
	TArray<FString> res;
	FString path = FPaths::ConvertRelativePathToFull(FPaths::GameContentDir()) + "Resources/Models/*.iordpt";

	res = this->listFiles(path);

	return res;
}

// List testing models
TArray<FString> AIInterface::listTestModels(int type) {
	TArray<FString> res;

	FString path;

	// Check type
	switch (type) {
		case 0:
			path = FPaths::ConvertRelativePathToFull(FPaths::GameContentDir()) + "Resources/Networks/Classification/Occupancy/Models/Test/*.iordtm";
			break;
		case 1:
			path = FPaths::ConvertRelativePathToFull(FPaths::GameContentDir()) + "Resources/Networks/Classification/Density/Models/Test/*.iordtm";
			break;
		case 2:
			path = FPaths::ConvertRelativePathToFull(FPaths::GameContentDir()) + "Resources/Networks/Classification/Normals/Models/Test/*.iordtm";
			break;
	}

	res = this->listFiles(path);

	return res;
}
// List training sets
TArray<FString> AIInterface::listTrainSets(int type, int mode) {
	TArray<FString> res;

	FString path;
	FString trainingMode;

	// Check training mode
	switch (mode) {
		case 0:
			trainingMode = "Simple/";
			break;
		case 1:
			trainingMode = "Advanced/";
			break;
		case 2:
			trainingMode = "Custom/";
			break;
	}

	// Check type
	switch (type) {
		case 0:
			path = FPaths::ConvertRelativePathToFull(FPaths::GameContentDir()) + "Resources/Networks/Classification/Occupancy/Models/Train/" + trainingMode + "*.iordts";
			break;
		case 1:
			path = FPaths::ConvertRelativePathToFull(FPaths::GameContentDir()) + "Resources/Networks/Classification/Density/Models/Train/" + trainingMode + "*.iordts";
			break;
		case 2:
			path = FPaths::ConvertRelativePathToFull(FPaths::GameContentDir()) + "Resources/Networks/Classification/Normals/Models/Train/" + trainingMode + "*.iordts";
			break;
	}

	res = this->listFiles(path);

	return res;
}



// List models from "classes.iordcore" training models
TArray<FString> AIInterface::listTrainDescriptions(int type, int mode) {
	TArray<FString> res;

	FString path;
	FString trainingMode;

	// Check training mode
	switch (mode) {
		case 0:
			trainingMode = "Simple/";
			break;
		case 1:
			trainingMode = "Advanced/";
			break;
		case 2:
			trainingMode = "Custom/";
			break;
	}

	// Check type
	switch (type) {
		case 0:
			path = FPaths::ConvertRelativePathToFull(FPaths::GameContentDir()) + "Resources/Networks/Classification/Occupancy/Models/Train/" + trainingMode + "classes.iordcore";
			break;
		case 1:
			path = FPaths::ConvertRelativePathToFull(FPaths::GameContentDir()) + "Resources/Networks/Classification/Density/Models/Train/" + trainingMode + "classes.iordcore";
			break;
		case 2:
			path = FPaths::ConvertRelativePathToFull(FPaths::GameContentDir()) + "Resources/Networks/Classification/Normals/Models/Train/" + trainingMode + "classes.iordcore";
			break;
	}

	// Get lines from textfile
	// Write description to "classes.iordcore"
	std::ifstream existFile(*path);
	std::string line;

	// Count file lines
	while (std::getline(existFile, line)) res.Add(line.c_str());
	existFile.close();

	return res;
}

TArray<FString> AIInterface::listFiles(FString path) {
	TArray<FString> res;

	IFileManager::Get().FindFiles(res, *path, true,   // to list files
		                                      false); // to skip directories

	return res;
}

// ---------------- //
// ----- EXEC ----- //
// ---------------- //

int AIInterface::train(int type, int mode) {
	FString gameContent = FPaths::ConvertRelativePathToFull(FPaths::GameContentDir());
	std::string stringGameContent(TCHAR_TO_UTF8(*gameContent));
	std::string cmd;

	std::string x;
	int retval = -1;

	switch (type) {
		case OCCUPANCY:
			switch (mode) {
				case SIMPLE:
					cmd = "py " + stringGameContent + "Resources/Networks/Classification/Occupancy/Scripts/LE_OCCUPANCY_TRAIN.py " + stringGameContent + "Resources/Networks/Classification/Occupancy/Scripts/ Simple";
					x = this->exec(cmd.c_str());
					retval = atoi(x.c_str());
					break;
				case ADVANCED:
					cmd = "py " + stringGameContent + "Resources/Networks/Classification/Occupancy/Scripts/LE_OCCUPANCY_TRAIN.py " + stringGameContent + "Resources/Networks/Classification/Occupancy/Scripts/ Advanced";
					x = this->exec(cmd.c_str());
					retval = atoi(x.c_str());
					break;
				case CUSTOM:
					cmd = "py " + stringGameContent + "Resources/Networks/Classification/Occupancy/Scripts/LE_OCCUPANCY_TRAIN.py " + stringGameContent + "Resources/Networks/Classification/Occupancy/Scripts/ Custom";
					x = this->exec(cmd.c_str());
					retval = atoi(x.c_str());
					break;
			}
			break;
		case DENSITY:
			switch (mode) {
				case SIMPLE:
					cmd = "py " + stringGameContent + "Resources/Networks/Classification/Density/Scripts/LE_DENSITY_TRAIN.py " + stringGameContent + "Resources/Networks/Classification/Density/Scripts/ Simple";
					x = this->exec(cmd.c_str());
					retval = atoi(x.c_str());
					break;
				case ADVANCED:
					cmd = "py " + stringGameContent + "Resources/Networks/Classification/Density/Scripts/LE_DENSITY_TRAIN.py " + stringGameContent + "Resources/Networks/Classification/Density/Scripts/ Advanced";
					x = this->exec(cmd.c_str());
					retval = atoi(x.c_str());
					break;
				case CUSTOM:
					cmd = "py " + stringGameContent + "Resources/Networks/Classification/Density/Scripts/LE_DENSITY_TRAIN.py " + stringGameContent + "Resources/Networks/Classification/Density/Scripts/ Custom";
					x = this->exec(cmd.c_str());
					retval = atoi(x.c_str());
					break;
			}
			break;
		case NORMALS:
			switch (mode) {
				case SIMPLE:
					cmd = "py " + stringGameContent + "Resources/Networks/Classification/Normals/Scripts/LE_VECTOR_TRAIN.py " + stringGameContent + "Resources/Networks/Classification/Normals/Scripts/ Simple";
					x = this->exec(cmd.c_str());
					retval = atoi(x.c_str());
					break;
				case ADVANCED:
					cmd = "py " + stringGameContent + "Resources/Networks/Classification/Normals/Scripts/LE_VECTOR_TRAIN.py " + stringGameContent + "Resources/Networks/Classification/Normals/Scripts/ Advanced";
					x = this->exec(cmd.c_str());
					retval = atoi(x.c_str());
					break;
				case CUSTOM:
					cmd = "py " + stringGameContent + "Resources/Networks/Classification/Normals/Scripts/LE_VECTOR_TRAIN.py " + stringGameContent + "Resources/Networks/Classification/Normals/Scripts/ Custom";
					x = this->exec(cmd.c_str());
					retval = atoi(x.c_str());
					break;
			}
			break;
	}

	// Write train mode to "mode.iordcore" file
	this->writeTrainMode(type, mode);

	return retval;
}

TMap<FString, float> AIInterface::test(int type, FString model) {
	TMap<FString, float> res;

	FString gameContent = FPaths::ConvertRelativePathToFull(FPaths::GameContentDir());
	std::string stringGameContent(TCHAR_TO_UTF8(*gameContent));
	std::string stringModel(TCHAR_TO_UTF8(*model));
	std::string cmd;

	std::string x;

	// Read train mode from "mode.iordcore" file
	int mode = this->readTrainMode(type);

	// Check if network is trained
	if (mode > -1) {
		TArray<FString> models = this->listTrainDescriptions(type, mode);
		TArray<float> probs;

		switch (type) {
			case OCCUPANCY:
				cmd = "py " + stringGameContent + "Resources/Networks/Classification/Occupancy/Scripts/LE_OCCUPANCY_TEST.py " + stringGameContent + "Resources/Networks/Classification/Occupancy/Scripts/ " + stringModel;
				x = this->exec(cmd.c_str());
				break;
			case DENSITY:
				cmd = "py " + stringGameContent + "Resources/Networks/Classification/Density/Scripts/LE_DENSITY_TEST.py " + stringGameContent + "Resources/Networks/Classification/Density/Scripts/ " + stringModel;
				x = this->exec(cmd.c_str());
				break;
			case NORMALS:
				cmd = "py " + stringGameContent + "Resources/Networks/Classification/Normals/Scripts/LE_VECTOR_TEST.py " + stringGameContent + "Resources/Networks/Classification/Normals/Scripts/ " + stringModel;
				x = this->exec(cmd.c_str());
				break;
		}

		std::istringstream iss(x);
		std::string word;
		float tmp;

		int i = 0;
		while (iss >> word) {
			if (i == 0) {
				i++;
				continue;
			}
			tmp = atof(word.c_str()) * 100.0f;
			probs.Add(tmp);
		}

		// Construct map
		int keySize = models.Num();
		int valueSize = probs.Num();
		if (keySize == valueSize) {
			for (i = 0; i < keySize; i++)
				res.Add(models[i], probs[i]);
		}

		// Order map
	}

	return res;
}

// ------------------------ //
// ----- FILE MANAGER ----- //
// ------------------------ //

int AIInterface::readTrainMode(int type) {
	int res;

	FString path;

	// Check type
	switch (type) {
		case 0:
			path = FPaths::ConvertRelativePathToFull(FPaths::GameContentDir()) + "Resources/Networks/Classification/Occupancy/Models/Train/mode.iordcore";
			break;
		case 1:
			path = FPaths::ConvertRelativePathToFull(FPaths::GameContentDir()) + "Resources/Networks/Classification/Density/Models/Train/mode.iordcore";
			break;
		case 2:
			path = FPaths::ConvertRelativePathToFull(FPaths::GameContentDir()) + "Resources/Networks/Classification/Normals/Models/Train/mode.iordcore";
			break;
	}

	// Get lines from textfile
	// Write description to "classes.iordcore"
	std::ifstream existFile(*path);
	std::string line;

	// Check if file exists
	if (existFile.good()) {
		// Read line
		std::getline(existFile, line);

		// Convert to int
		res = atoi(line.c_str());
	}
	else {
		res = -1;
	}

	return res;
}

void AIInterface::writeTrainMode(int type, int mode) {
	FString path;

	std::string modeString = std::to_string(mode);

	// Check type
	switch (type) {
		case 0:
			path = FPaths::ConvertRelativePathToFull(FPaths::GameContentDir()) + "Resources/Networks/Classification/Occupancy/Models/Train/mode.iordcore";
			break;
		case 1:
			path = FPaths::ConvertRelativePathToFull(FPaths::GameContentDir()) + "Resources/Networks/Classification/Density/Models/Train/mode.iordcore";
			break;
		case 2:
			path = FPaths::ConvertRelativePathToFull(FPaths::GameContentDir()) + "Resources/Networks/Classification/Normals/Models/Train/mode.iordcore";
			break;
	}

	// Remove file
	this->deleteFile(path);

	// Create file
	std::ofstream classesFile;
	classesFile.open(*path, std::ios_base::app);
	classesFile << modeString << "\n";
	classesFile.close();
}

void AIInterface::deleteSelectedModel(int type, FString filename) {
	FString path;

	// Check type
	switch (type) {
		case 0:
			path = FPaths::ConvertRelativePathToFull(FPaths::GameContentDir()) + "Resources/Networks/Classification/Occupancy/Models/Test/" + filename;
			break;
		case 1:
			path = FPaths::ConvertRelativePathToFull(FPaths::GameContentDir()) + "Resources/Networks/Classification/Density/Models/Test/" + filename;
			break;
		case 2:
			path = FPaths::ConvertRelativePathToFull(FPaths::GameContentDir()) + "Resources/Networks/Classification/Normals/Models/Test/" + filename;
			break;
	}

	// UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Removing file '%s' ..."), *path);
	this->deleteFile(path);
}

void AIInterface::deleteSelectedSet(int type, int mode, FString filename) {
	FString path;
	FString trainingMode;

	// Check training mode
	switch (mode) {
	case 0:
		trainingMode = "Simple/";
		break;
	case 1:
		trainingMode = "Advanced/";
		break;
	case 2:
		trainingMode = "Custom/";
		break;
	}

	// Check type
	switch (type) {
	case 0:
		path = FPaths::ConvertRelativePathToFull(FPaths::GameContentDir()) + "Resources/Networks/Classification/Occupancy/Models/Train/" + trainingMode + filename;
		break;
	case 1:
		path = FPaths::ConvertRelativePathToFull(FPaths::GameContentDir()) + "Resources/Networks/Classification/Density/Models/Train/" + trainingMode + filename;
		break;
	case 2:
		path = FPaths::ConvertRelativePathToFull(FPaths::GameContentDir()) + "Resources/Networks/Classification/Normals/Models/Train/" + trainingMode + filename;
		break;
	}

	this->deleteFile(path);
}

void AIInterface::deleteSelectedCloudPoint(FString filename) {
	FString path = FPaths::ConvertRelativePathToFull(FPaths::GameContentDir()) + "Resources/Models/" + filename;
	this->deleteFile(path);
}

void AIInterface::deleteFile(FString path) {
	std::string pathString(TCHAR_TO_UTF8(*path));
	std::remove(pathString.c_str());
}

// ----------------------------- //
// ----- AUXILIARY METHODS ----- //
// ----------------------------- //

std::string AIInterface::exec(const char* cmd) {
	std::array<char, 128> buffer;
	std::string result;

	std::shared_ptr<FILE> pipe(_popen(cmd, "r"), _pclose);
	if (!pipe)
		throw std::runtime_error("popen() failed!");
	while (!feof(pipe.get())) {
		if (fgets(buffer.data(), 128, pipe.get()) != NULL)
			result += buffer.data();
	}
	return result;
}

// ---------------- //
// ----- DRAW ----- //
// ---------------- //
void AIInterface::drawCoordinates() {
	FColor red = FColor(150, 0, 0);
	FColor green = FColor(0, 100, 0);
	FColor blue = FColor(0, 0, 100);

	FVector center = FVector(0.0f);

	// Draw orientation X, Y and Z
	// X
	DrawDebugDirectionalArrow(
		GetWorld(),
		center,
		center + 10,
		2,
		red,
		1000
	);

	// Y
	DrawDebugDirectionalArrow(
		GetWorld(),
		center,
		center + 10,
		2,
		green,
		1000
	);

	// Z
	DrawDebugDirectionalArrow(
		GetWorld(),
		center,
		center + 10,
		2,
		blue,
		1000
	);
}

