// Fill out your copyright notice in the Description page of Project Settings.

#include "IORD.h"
#include "Voxel.h"

// ----- CONSTRUCTORS ----- //

Voxel::Voxel()
{
	this->i = 0;
	this->j = 0;
	this->k = 0;
	this->size = 0;
}

Voxel::Voxel(int a, int b, int c)
{
	this->i = a;
	this->j = b;
	this->k = c;
	this->size = 0;
}

Voxel::Voxel(int a, int b, int c, float s)
{
	this->i = a;
	this->j = b;
	this->k = c;
	this->size = s;
}

Voxel::~Voxel()
{
	// Nothing to do
}

// ----- METHODS ----- //

int Voxel::getI() {
	return this->i;
}

int Voxel::getJ() {
	return this->j;
}

int Voxel::getK() {
	return this->k;
}

FVector Voxel::getCentroid()
{
	return FVector(this->size * i, this->size * j, this->size * k);
}

float Voxel::getSize() {
	return this->size;
}

TArray<FVector> Voxel::getPoints() {
	return this->points;
}

void Voxel::clearPoints() {
	this->points.Empty();
}

int Voxel::getNumPoints() {
	return this->points.Num();
}

void Voxel::addPoint(FVector point)
{
	this->points.AddUnique(point);
}

bool Voxel::isOccupied()
{
	return this->getNumPoints() > 0;
}
