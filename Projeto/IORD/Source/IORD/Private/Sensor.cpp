// Fill out your copyright notice in the Description page of Project Settings.

#include "IORD.h"
#include "Sensor.h"

// Sets default values
ASensor::ASensor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ASensor::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ASensor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// ------------------ //
// ----- CONFIG ----- //
// ------------------ //

void ASensor::clear() {
	this->back.Empty();
	this->object.Empty();
}

void ASensor::initKinect(FColor c1, FColor c2, bool nearMode, float t) {
	int numSensors;

	// Get a working kinect sensor
	if ((NuiGetSensorCount(&numSensors) < 0 || numSensors < 1) || (NuiCreateSensorByIndex(0, &this->sensor) < 0)) 
		this->active = false;
	else {
		this->width = 320;
		this->height = 240;

		// Initialize sensor
		sensor->NuiInitialize(NUI_INITIALIZE_FLAG_USES_DEPTH);

		if (nearMode) {
			sensor->NuiImageStreamOpen(
				NUI_IMAGE_TYPE_DEPTH,	                    // Depth camera
				NUI_IMAGE_RESOLUTION_320x240,               // Image resolution
				NUI_IMAGE_STREAM_FLAG_ENABLE_NEAR_MODE,		// Image stream flags, e.g. near mode
				2,                                          // Number of frames to buffer
				NULL,                                       // Event handle
				&this->depthStream);
		}
		else {
			sensor->NuiImageStreamOpen(
				NUI_IMAGE_TYPE_DEPTH,	                    // Depth camera
				NUI_IMAGE_RESOLUTION_320x240,               // Image resolution
				0,											// Image stream flags, e.g. near mode
				2,                                          // Number of frames to buffer
				NULL,                                       // Event handle
				&this->depthStream);
		}

		// Activated?
		this->active = this->sensor;
	}

	// Clear values
	this->clear();

	this->backColor = c1;
	this->objectColor = c2;

	this->tolerance = t;
}

// ---------------- //
// ----- DRAW ----- //
// ---------------- //

void ASensor::drawBackground() {
	// Draw background
	for (FVector v : this->back) {
		DrawDebugPoint(
			GetWorld(),
			v,
			2,  	// Size
			this->backColor,
			1000    // Duration
		);
	}
}

void ASensor::drawObject() {
	// Draw object
	for (TArray<FVector> l : this->object) {
		this->drawList(l);
	}
}

void ASensor::drawList(TArray<FVector> a) {
	// Draw object
	for (FVector v : a) {
		DrawDebugPoint(
			GetWorld(),
			v,
			2,  	// Size
			this->objectColor,
			1000    // Duration
		);
	}
}

// ------------------ //
// ----- PHOTOS ----- //
// ------------------ //

void ASensor::getBackground() {
	this->back = this->getPoints();
}

void ASensor::takePhoto(bool rotate, float angle) {
	TArray<FVector> photo = this->getPoints();
	TArray<FVector> diff;
	bool close;

	// Make difference to background
	for (FVector p : photo) {
		close = false;

		for (FVector b : this->back) {
			if (FVector::Dist(p, b) < this->tolerance) {
				close = true;
				break;
			}
		}

		// Add point if not close
		if (!close) {
			diff.AddUnique(p);
		}
	}

	// Center difference
	diff = this->center(diff);

	// Rotate points ?
	int size = this->object.Num();
	if (rotate) 
		diff = this->rotate(diff, size  * angle);

	// Add to object points
	this->object.Add(diff);
}

// ------------------ //
// ----- SAVE ------- //
// ------------------ //

void ASensor::saveModel(FString filename, FString description) {
	FString path = FPaths::ConvertRelativePathToFull(FPaths::GameContentDir()) + "Resources/Models/" + filename;
	FString JsonStr;
	TSharedRef <TJsonWriter<TCHAR>> JsonWriter = TJsonWriterFactory<>::Create(&JsonStr);

	// Local data
	FVector tempNormal;
	TArray<float> x, y, z;

	// Write data
	JsonWriter->WriteObjectStart();

	// -----
	int numPoints = 0;
	for (TArray<FVector> l : this->object) {
		numPoints += l.Num();
		for (FVector p : l) {
			x.Add(p.X);
			y.Add(p.Y);
			z.Add(p.Z);
		}
	}

	JsonWriter->WriteObjectStart("model");
	JsonWriter->WriteValue("description", description);
	JsonWriter->WriteValue("points", numPoints);

	// LIST X
	JsonWriter->WriteArrayStart("x");
	for (float xValue : x) {
		JsonWriter->WriteValue((TEXT("%.2f"), xValue));
	}
	JsonWriter->WriteArrayEnd();

	// LIST Y
	JsonWriter->WriteArrayStart("y");
	for (float yValue : y) {
		JsonWriter->WriteValue((TEXT("%.2f"), yValue));
	}
	JsonWriter->WriteArrayEnd();

	// LIST Z
	JsonWriter->WriteArrayStart("z");
	for (float zValue : z) {
		JsonWriter->WriteValue((TEXT("%.2f"), zValue));
	}
	JsonWriter->WriteArrayEnd();

	JsonWriter->WriteObjectEnd();
	// -----

	JsonWriter->WriteObjectEnd();
	JsonWriter->Close();

	// Save string
	UE_LOG(LogTemp, Warning, TEXT("[DEBUG] Writing to file '%s'"), *path);
	FFileHelper::SaveStringToFile(*JsonStr, *path);
}

TArray<FVector> ASensor::center(TArray<FVector> a) {
	TArray<FVector> res;
	FVector objectCenter = this->centroid(a);

	res = this->translate(a, -objectCenter.X, -objectCenter.Y, -objectCenter.Z);

	return res;
}

// ----- AUXILIARY METHODS ----- //

// Count how many kinect sensors are currently connected to the system
int ASensor::countKinect() {
	int count = 0;
	NuiGetSensorCount(&count);

	UE_LOG(LogTemp, Warning, TEXT("[KINECT] Found %d kinects ..."), count);
	return count;
}

TArray<USHORT> ASensor::getDepth() {
	TArray<USHORT> res;

	NUI_IMAGE_FRAME imageFrame;
	NUI_LOCKED_RECT LockedRect;

	// Check if kinect can fetch next frame
	if (this->sensor->NuiImageStreamGetNextFrame(this->depthStream, 0, &imageFrame) < 0) {
		UE_LOG(LogTemp, Warning, TEXT("     [ERROR] No frame caught ..."));
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("     Getting depth from frame ..."));

		INuiFrameTexture* texture = imageFrame.pFrameTexture;
		texture->LockRect(0, &LockedRect, NULL, 0);

		if (LockedRect.Pitch != 0) {
			const USHORT* curr = (const USHORT*) LockedRect.pBits;

			// Check if NULL
			if (curr) {
				for (int j = 0; j < this->height; ++j) {
					for (int i = 0; i < this->width; ++i) {
						// Get depth of pixel in millimeters
						USHORT depth = NuiDepthPixelToDepth(*curr++);

						// Get pixel
						res.Add(depth);
					}
				}
			}
			else {
				UE_LOG(LogTemp, Warning, TEXT("     [ERROR] Error extracting information from frame ..."));
			}
		}

		texture->UnlockRect(0);
		this->sensor->NuiImageStreamReleaseFrame(this->depthStream, &imageFrame);
	}

	return res;
}

TArray<FVector> ASensor::getPoints() {
	TArray<FVector> res;

	Vector4 tempSomething;
	FVector tempPos;

	NUI_IMAGE_FRAME imageFrame;
	NUI_LOCKED_RECT LockedRect;

	// Check if kinect can fetch next frame
	if (this->sensor->NuiImageStreamGetNextFrame(this->depthStream, 0, &imageFrame) < 0) {
		UE_LOG(LogTemp, Warning, TEXT("     [ERROR] No frame caught ..."));
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("     [DEBUG] Getting points from frame ..."));

		INuiFrameTexture* texture = imageFrame.pFrameTexture;
		texture->LockRect(0, &LockedRect, NULL, 0);

		if (LockedRect.Pitch != 0) {
			const USHORT* curr = (const USHORT*) LockedRect.pBits;

			// Check if NULL
			if (curr) {
				for (int j = 0; j < this->height; j++) {
					for (int i = 0; i < this->width; i++) {
						// Get depth of pixel in millimeters
						USHORT depth = NuiDepthPixelToDepth(*curr++);

						// Store coordinates of the point corresponding to this pixel
						tempSomething = NuiTransformDepthImageToSkeleton(i, j, depth << 3, NUI_IMAGE_RESOLUTION_320x240);
						tempPos = FVector((tempSomething.z / tempSomething.w) * 100,
							              (tempSomething.x / tempSomething.w) * 100,
										  (tempSomething.y / tempSomething.w) * 100);
						res.AddUnique(tempPos);
					}
				}
			}
			else {
				UE_LOG(LogTemp, Warning, TEXT("     [ERROR] Error extracting information from frame ..."));
			}
		}

		texture->UnlockRect(0);
		this->sensor->NuiImageStreamReleaseFrame(this->depthStream, &imageFrame);
	}

	return res;
}

TArray<FVector> ASensor::translate(TArray<FVector> a, float x, float y, float z) {
	TArray<FVector> res;

	for (FVector v : a) {
		res.Add(FVector(v.X + x, v.Y + y, v.Z + z));
	}

	return res;
}

TArray<FVector> ASensor::rotate(TArray<FVector> a, float angle) {
	// x' = x * cos(v) - y * sin(v)
	// y' = x * sin(v) + y * cos(v)
	// z' = z

	TArray<FVector> res;

	FVector yAxis = FVector(0.0f, 1.0f, 0.0f);
	FVector zAxis = FVector(0.0f, 0.0f, 1.0f);
	FVector temp;

	float radian = FMath::DegreesToRadians(angle);

	for (FVector v : a) {
		temp = v.RotateAngleAxis(30, yAxis);
		temp = temp.RotateAngleAxis(angle, zAxis);
		res.Add(temp);
	}

	return res;
}

FVector ASensor::centroid(TArray<FVector> a) {
	int count = a.Num();
	FVector sum = FVector(0.0f);

	for (FVector v : a) {
		sum += v;
	}

	sum /= count;

	return sum;
}

