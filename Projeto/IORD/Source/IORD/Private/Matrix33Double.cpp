// Fill out your copyright notice in the Description page of Project Settings.

#include "IORD.h"
#include "Matrix33Double.h"

Matrix33Double::Matrix33Double()
{
	// Nothing to do
}

Matrix33Double::Matrix33Double(double x1, double x2, double x3, double y1, double y2, double y3, double z1, double z2, double z3) {
	// First row
	this->mat[0][0] = x1;
	this->mat[0][1] = x2;
	this->mat[0][2] = x3;

	// Second row
	this->mat[1][0] = y1;
	this->mat[1][1] = y2;
	this->mat[1][2] = y3;

	// Third row
	this->mat[2][0] = z1;
	this->mat[2][1] = z2;
	this->mat[2][2] = z3;
}

Matrix33Double::Matrix33Double(double m[3][3]) {
	for (int i = 0; i < 3; i++) {
		this->mat[i][0] = m[i][0];
		this->mat[i][1] = m[i][1];
		this->mat[i][2] = m[i][2];
	}
}

Matrix33Double::~Matrix33Double()
{
	// Nothing to do
}

double* Matrix33Double::getLine(int position) {
	return mat[position];
}

double* Matrix33Double::getColumn(int position) {
	double* res = new double[3];

	if (position < 3) {
		res[0] = this->mat[0][position];
		res[1] = this->mat[1][position];
		res[2] = this->mat[2][position];
	}

	return res;
}

double Matrix33Double::getDeterminant() {
	// a(ei - fh) - b(di - fg) + c(dh - eg)

	// First row
	double x1 = this->mat[0][0]; //a
	double x2 = this->mat[0][1]; //b
	double x3 = this->mat[0][2]; //c

								// Second row
	double y1 = this->mat[1][0]; //d
	double y2 = this->mat[1][1]; //e
	double y3 = this->mat[1][2]; //f

								// Third row
	double z1 = this->mat[2][0]; //g
	double z2 = this->mat[2][1]; //h
	double z3 = this->mat[2][2]; //i

	return x1 * (y2 * z3 - y3 * z2) - x2 * (y1 * z3 - y3 * z1) + x3 * (y1 * z2 - y2 * z1);
}

Matrix33Double Matrix33Double::getInverse() {
	int i, j, k;
	Matrix33Double temp = Matrix33Double(this->mat);
	Matrix33Double x = Matrix33Double();
	Matrix33Double b = Matrix33Double();
	int* index;

	// Create identity matrix
	for (i = 0; i < 3; i++)
		b.mat[i][i] = 1;

	// Transform the matrix into an upper triangle
	index = temp.gaussianPivoting();

	// Update the matrix b[i][j] with the ratios stored
	for (i = 0; i < 2; i++)
		for (j = i + 1; j < 3; j++)
			for (k = 0; k < 3; k++)
				b.mat[index[j]][k] -= temp.mat[index[j]][i] * b.mat[index[i]][k];

	// Perform backward substitutions
	for (i = 0; i < 3; i++)
	{
		x.mat[2][i] = b.mat[index[2]][i] / temp.mat[index[2]][2];
		for (j = 1; j >= 0; j--)
		{
			x.mat[j][i] = b.mat[index[j]][i];
			for (k = j + 1; k < 3; k++) {
				x.mat[j][i] -= temp.mat[index[j]][k] * x.mat[k][i];
			}
			x.mat[j][i] /= temp.mat[index[j]][j];
		}
	}

	return x;
}

FVector Matrix33Double::findEigenVectorAssociatedWithLargestEigenValue(int numIterations) {
	// Pre condition
	double s = findLargestEntry();
	Matrix33Double mc = scale(1.0 / s);
	mc = mc.power(3);

	FVector v = FVector(1.0f);
	FVector lastV = v;
	for (int i = 0; i < numIterations; i++) {
		v = mc.multiply(v);
		if (FVector::DistSquared(v, lastV) < 1e-16f) {
			break;
		}
		lastV = v;
	}

	// Return rounded normals to 1 decimal place
	return FVector(FMath::RoundToFloat(v.X * 10.0f) / 10.0f, FMath::RoundToFloat(v.Y * 10.0f) / 10.0f, FMath::RoundToFloat(v.Z * 10.0f) / 10.0f);
}

Matrix33Double Matrix33Double::scale(double value) {
	Matrix33Double temp = Matrix33Double(this->mat);

	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			temp.mat[i][j] *= value;
		}
	}

	return temp;
}

Matrix33Double Matrix33Double::power(int value) {
	Matrix33Double res = Matrix33Double(this->mat);

	for (int i = 0; i < value; i++) {
		res = res.multiply(res);
	}

	return res;
}

Matrix33Double Matrix33Double::multiply(Matrix33Double m) {
	double res[3][3];

	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			res[i][j] = multiply(this->getLine(i), m.getColumn(j));
		}
	}

	return Matrix33Double(res);
}

FVector Matrix33Double::multiply(FVector v) {
	FVector res;
	double temp[3] = { v.X, v.Y, v.Z };

	res.X = multiply(this->getLine(0), temp);
	res.Y = multiply(this->getLine(1), temp);
	res.Z = multiply(this->getLine(2), temp);
	res.Normalize();

	return res;
}

double Matrix33Double::multiply(double* line, double* column) {
	return line[0] * column[0] + line[1] * column[1] + line[2] * column[2];
}

FString Matrix33Double::print() {
	FString res;

	// First row
	double x1 = this->mat[0][0];
	double x2 = this->mat[0][1];
	double x3 = this->mat[0][2];

	// Second row
	double y1 = this->mat[1][0];
	double y2 = this->mat[1][1];
	double y3 = this->mat[1][2];

	// Third row
	double z1 = this->mat[2][0];
	double z2 = this->mat[2][1];
	double z3 = this->mat[2][2];

	res = FString::Printf(TEXT("\n{ %.6f, %.6f, %.6f,\n  %.6f, %.6f, %.6f,\n  %.6f, %.6f, %.6f }"), x1, x2, x3, y1, y2, y3, z1, z2, z3);

	return res;
}

// ----- AUXILIARY METHODS ----- //

// Method to carry out the partial-pivoting Gaussian-elimination
int* Matrix33Double::gaussianPivoting() {
	int i, j, k, l;
	double c0, c1;
	double pi0, pi1;
	double pj;
	int itmp;

	int* index = new int[3];
	double c[3];

	// Initialize the index
	for (i = 0; i < 3; i++)
		index[i] = i;

	// Find the rescaling factors, one from each row
	for (i = 0; i < 3; i++)
	{
		c1 = 0.0;
		for (j = 0; j < 3; ++j) {
			c0 = FMath::Abs(this->mat[i][j]);
			if (c0 > c1)
				c1 = c0;
		}
		c[i] = c1;
	}

	// Search the pivoting element from each column
	k = 0;
	for (j = 0; j < 2; j++)
	{
		pi1 = 0.0;
		for (i = j; i < 3; i++)
		{
			pi0 = FMath::Abs(this->mat[index[i]][j]) / c[index[i]];
			if (pi0 > pi1) {
				pi1 = pi0;
				k = i;
			}
		}

		// Interchange rows according to the pivoting order
		itmp = index[j];
		index[j] = index[k];
		index[k] = itmp;

		for (i = j + 1; i < 3; i++)
		{
			pj = this->mat[index[i]][j] / this->mat[index[j]][j];

			// Record pivoting ratios below the diagonal
			this->mat[index[i]][j] = pj;

			// Modify other elements accordingly
			for (l = j + 1; l < 3; l++)
				this->mat[index[i]][l] -= pj * this->mat[index[j]][l];
		}
	}

	return index;
}

double Matrix33Double::findLargestEntry() {
	int i, j;
	double entry;
	double result = 0.0;

	for (i = 0; i < 3; i++) {
		for (j = 0; j < 3; j++) {
			entry = FMath::Abs(this->mat[i][j]);
			result = FMath::Max(entry, result);
		}
	}

	return result;
}
